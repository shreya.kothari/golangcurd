package database

import (
	"database/sql"

	"GoLangCURD/models"
)

//GetAllStudents function
func (db Database) GetAllStudents() (*models.StudentList, error) {
	list := &models.StudentList{}
	rows, err := db.Conn.Query(`SELECT * FROM public."Student"	ORDER BY "ID" ASC `)
	if err != nil {
		return list, err
	}
	for rows.Next() {
		var student models.Student
		err := rows.Scan(&student.Name, &student.RollNo, &student.ID)
		if err != nil {
			return list, err
		}
		list.Students = append(list.Students, student)
	}
	return list, nil
}

//AddStudent function
func (db Database) AddStudent(Student *models.Student) error {
	var id int

	query := `insert into public."Student"("Name", "RollNo") values($1, $2) RETURNING "ID" `
	err := db.Conn.QueryRow(query, Student.Name, Student.RollNo).Scan(&id)
	if err != nil {
		return err
	}
	Student.ID = id

	return nil
}

//GetStudentByID ...
func (db Database) GetStudentByID(studentID int) (models.Student, error) {
	student := models.Student{}
	query := `SELECT * FROM public."Student" where "ID" = $1 `
	row := db.Conn.QueryRow(query, studentID)
	switch err := row.Scan(&student.Name, &student.RollNo, &student.ID); err {
	case sql.ErrNoRows:
		return student, ErrNoMatch
	default:
		return student, err
	}
}

//DeleteStudent ...
func (db Database) DeleteStudent(studentID int) error {
	query := `DELETE FROM public."Student" WHERE "ID" = $1;`
	_, err := db.Conn.Exec(query, studentID)
	switch err {
	case sql.ErrNoRows:
		return ErrNoMatch
	default:
		return err
	}
}

//UpdateStudent ...
func (db Database) UpdateStudent(studentID int, StudentData models.Student) (models.Student, error) {
	student := models.Student{}
	query := `UPDATE public."Student" SET "Name"=$1, "RollNo"=$2 WHERE "ID"=$3 RETURNING "ID", "Name", "RollNo";`
	err := db.Conn.QueryRow(query, StudentData.Name, StudentData.RollNo, studentID).Scan(&student.ID, &student.Name, &student.RollNo)
	if err != nil {
		if err == sql.ErrNoRows {
			return student, ErrNoMatch
		}
		return student, err
	}
	return student, nil
}
