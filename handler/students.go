package handler

import (
	"GoLangCURD/database"
	"GoLangCURD/models"
	"context"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type key int

const (
	studentIDKey key = iota
)

func students(router chi.Router) {
	router.Get("/", getAllStudents)
	router.Post("/", createStudent)
	router.Route("/{studentID}", func(router chi.Router) {
		router.Use(StudentContext)
		router.Get("/", getStudent)
		router.Put("/", updateStudent)
		router.Delete("/", deleteStudent)
	})
}

//StudentContext ...
func StudentContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		studentID := chi.URLParam(r, "studentID")
		if studentID == "" {
			render.Render(w, r, ErrorRenderer(fmt.Errorf("Student ID is required")))
			return
		}
		id, err := strconv.Atoi(studentID)

		if err != nil {
			render.Render(w, r, ErrorRenderer(fmt.Errorf("invalid Student ID")))
		}
		ctx := context.WithValue(r.Context(), studentIDKey, id)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func getAllStudents(w http.ResponseWriter, r *http.Request) {
	students, err := dbInstance.GetAllStudents()
	if err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		return
	}
	if err := render.Render(w, r, students); err != nil {
		render.Render(w, r, ErrorRenderer(err))
	}
}
func getStudent(w http.ResponseWriter, r *http.Request) {
	studentID := r.Context().Value(studentIDKey).(int)
	student, err := dbInstance.GetStudentByID(studentID)
	if err != nil {
		if err == database.ErrNoMatch {
			render.Render(w, r, ErrNotFound)
		} else {
			render.Render(w, r, ErrorRenderer(err))
		}
		return
	}
	if err := render.Render(w, r, &student); err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		return
	}
}
func createStudent(w http.ResponseWriter, r *http.Request) {
	student := &models.Student{}
	if err := render.Bind(r, student); err != nil {
		render.Render(w, r, ErrBadRequest)
		return
	}
	if err := dbInstance.AddStudent(student); err != nil {
		render.Render(w, r, ErrorRenderer(err))
		return
	}
	if err := render.Render(w, r, student); err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		return
	}
}

func deleteStudent(w http.ResponseWriter, r *http.Request) {
	studentID := r.Context().Value(studentIDKey).(int)
	err := dbInstance.DeleteStudent(studentID)
	if err != nil {
		if err == database.ErrNoMatch {
			render.Render(w, r, ErrNotFound)
		} else {
			render.Render(w, r, ServerErrorRenderer(err))
		}
		return
	}
}
func updateStudent(w http.ResponseWriter, r *http.Request) {
	studentID := r.Context().Value(studentIDKey).(int)
	studentData := models.Student{}
	if err := render.Bind(r, &studentData); err != nil {
		render.Render(w, r, ErrBadRequest)
		return
	}
	student, err := dbInstance.UpdateStudent(studentID, studentData)
	if err != nil {
		if err == database.ErrNoMatch {
			render.Render(w, r, ErrNotFound)
		} else {
			render.Render(w, r, ServerErrorRenderer(err))
		}
		return
	}
	if err := render.Render(w, r, &student); err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		return
	}
}
