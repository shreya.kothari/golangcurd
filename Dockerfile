FROM golang:1.14.6-alpine3.12 as builder

WORKDIR /goApp

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build \
    -ldflags "-X main.githash=$(git rev-parse HEAD) -X main.buildstamp=$(date +%Y%m%d.%H%M%S)" \
    -o golangCURD 

################################################

FROM alpine:latest

RUN apk add --no-cache ca-certificates && update-ca-certificates
WORKDIR /goApp


COPY --from=builder /goApp/golangCURD .
EXPOSE 8080 8080
CMD ["/goApp/golangCURD"]
ENTRYPOINT ["/goApp/golangCURD"]