package models

import (
	"fmt"
	"net/http"
)

//Student json structure
type Student struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	RollNo string `json:"rollNo"`
}

//StudentList json structure
type StudentList struct {
	Students []Student `json:"students"`
}

//Bind ...
func (i *Student) Bind(r *http.Request) error {
	if i.Name == "" {
		return fmt.Errorf("name is a required field")
	}
	return nil
}

//Render ...
func (*StudentList) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

//Render ...
func (*Student) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
